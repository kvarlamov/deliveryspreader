﻿using Blocks.Contracts.BaseEntitys;
using Microsoft.EntityFrameworkCore;
using OrderAPI.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrderAPI.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataContext _db;

        public EfRepository(DataContext db)
        {
            _db = db;
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync(string property)
        {
            return await _db.Set<T>().Include(property).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> GetByIdAsync(Guid id, string property)
        {
            return await _db.Set<T>().Include(property).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task CreateAsync(T t)
        {
            await _db.Set<T>().AddAsync(t);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateAsync(T t)
        {
            _db.Entry(t).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T t)
        {
            _db.Set<T>().Remove(t);
            await _db.SaveChangesAsync();
        }
    }
}
