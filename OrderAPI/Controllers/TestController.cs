﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace OrderAPI.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> _logger;

        public TestController(ILogger<TestController> logger)
        {
            _logger = logger;
        }
        /// <summary>
        /// test route
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpGet]
        [Route("test")]
        public string Test()
        {
            _logger.LogInformation("trying to get test");
            try
            {
                throw new Exception("test my formatter");
            }
            catch (Exception e)
            {
                _logger.LogError(e, "error");
                return e.Message;
            }
        }
    }
}