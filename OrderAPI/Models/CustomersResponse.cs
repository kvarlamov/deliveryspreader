﻿using Blocks.Contracts.OrdersManagement;
using System;

namespace OrderAPI.WebHost.Models
{
    public class CustomersResponse
    {
        public CustomersResponse(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            Email = customer.Email;
            Address = customer.Address;
        }

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }
    }
}
