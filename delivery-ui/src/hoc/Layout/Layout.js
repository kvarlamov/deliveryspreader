﻿import React from "react";
import classes from './Layout.module.css'
import {Sidebar} from "../../components/Navigation/Sidebar/Sidebar";

export const Layout = (props) => {
    return (
        <div className={classes.Layout}>
            <Sidebar/>
            <main>
                {props.children}
            </main>
        </div>
    )
    
}