import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../../axios/axios-delivery";

export const takeOrderAsync = createAsyncThunk(
    'takeorder/Take',
    async(item) => {
        const {id, orderId} = item;
        const response = await axios.put(`Couriers?id=${id}&orderId=${orderId}`);
        return response.data;
    }
)

export const takeorderSlice = createSlice({
    name: 'profile',
    initialState: {
        status: 'idle',
        error: null,
        
    },
    reducers: {
        
    },
    extraReducers(builder) {
        builder
            .addCase(takeOrderAsync.pending, state => {
                state.status = 'loading';
            })
            .addCase(takeOrderAsync.fulfilled, (state, action) => {
                state.status = 'success';
            })
            .addCase(takeOrderAsync.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
    }
})

export default takeorderSlice.reducer;