﻿import {combineReducers} from "@reduxjs/toolkit";
import orderReducer from "./orderListSlice";
import profileReducer from "./profileSlice";
import takeOrderReducer from "./takeOrderReducer";

export default combineReducers({
    order: orderReducer,
    profile: profileReducer,
    takeorder: takeOrderReducer
});