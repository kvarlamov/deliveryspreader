﻿import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../../axios/axios-order";

const initialState = {
    orders: [],
    ordersPerPage: 50,
    numOfPages: 1, //todo - need to get from server
    status: 'idle',
    error: null
}

export const fetchOrdersAsync = createAsyncThunk(
    'orderList/fetchOrders',
    async () => {
        const response = await axios("Orders");
        return response.data.filter(x => x.products.length !== 0);
    }
)

export const orderListSlice = createSlice({
    name: 'orderList',
    initialState,
    reducers: {},
    extraReducers(builder) {
        builder
            .addCase(fetchOrdersAsync.pending, (state, action) => {
                state.status = 'loading'
            })
            .addCase(fetchOrdersAsync.fulfilled, (state, action) => {
                state.status = 'success';
                state.numOfPages = Math.ceil( action.payload.length / state.ordersPerPage);
                //state.orders = state.orders.concat(action.payload);
                state.orders = [...action.payload];                
            })
            .addCase(fetchOrdersAsync.rejected, (state, action) => {
                state.status = 'failed';
                console.log(action);
                state.error = action.error.message;
            })
    }
});

// export const {fetchOrdersStart, fetchOrdersSuccess, fetchOrderError} = orderListSlice.actions;

// export const fetchOrdersAsync = () => async (dispatch) => {
//     dispatch(fetchOrdersStart);
//     try {
//         const response = await axios("Order");
//         console.log("orders after axios: ", response.data);
//         dispatch(fetchOrdersSuccess(response.data))
//     } catch (e) {
//         dispatch(fetchOrderError(e))
//     }    
// }

export default orderListSlice.reducer;