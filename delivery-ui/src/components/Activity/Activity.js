﻿import React from "react";
import styles from './Activity.module.css';
import {useSelector} from "react-redux";

export const Activity = () => { 
    const ordersInProgress = useSelector(state => state.profile.ordersInProgress)
    
    return (
        <div className={styles.Activity}>
            <h2>Orders info</h2>
            <p>Active orders: {ordersInProgress.length}</p>
        </div>
    )
}