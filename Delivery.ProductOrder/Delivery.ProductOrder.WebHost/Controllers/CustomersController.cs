﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Delivery.ProductOrder.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blocks.Contracts.OrdersManagement;
using Blocks.Contracts.Models;
using Blocks.Contracts.Abstractions;

namespace Delivery.ProductOrder.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {

        private readonly ILogger<CustomersController> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly ICachingService<Customer> _service;

        public CustomersController(ILogger<CustomersController> logger, IRepository<Customer> customerRepository, ICachingService<Customer> service)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            this._service = service;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomersResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x => new CustomersResponse(x)).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<CustomersResponse> GetCustomerByIdAsync(Guid id)
        {
            var customer = await _service.GetByIdAsync(id);
            return new CustomersResponse(customer);
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateCustomerAsync(CustomerRequest request)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                Email = request.Email
            };
            await _service.CreateAsync(customer);
            return Ok("Покупатель создан");
        }

        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateCustomerAsync(Customer customer)
        {
            await _service.UpdateAsync(customer);
            return Ok("Покупатель обновлён");
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteCustomerAsync(Guid id)
        {
            await _service.DeleteAsync(id);
            return Ok("Покупатель удалён");
        }

    }
}
