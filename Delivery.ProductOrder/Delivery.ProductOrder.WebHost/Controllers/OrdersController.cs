using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Delivery.ProductOrder.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductOrder.Integration.AsyncDataServices;
using Delivery.ProductOrder.DataAccess.Services;
using Blocks.Contracts.Dto;
using Blocks.Contracts.Models;
using Blocks.Contracts.Abstractions;

namespace Delivery.ProductOrder.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly ILogger<OrdersController> _logger;
        private readonly IRepository<Order> _orderRepository;
        private readonly ICachingService<Order> _cache;


        public OrdersController(
            ILogger<OrdersController> logger,
            IRepository<Order> orderRepository,
            ICachingService<Order> cache)
        {
            _logger = logger;
            _orderRepository = orderRepository;
            _cache = cache;
        }

        /// <summary>
        /// Получить все заказы вместе с продуктами, клиентом и назначенным курьером
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<OrdersResponce>>> GetOrdersAllAsync()
        {

            var orders = await _orderRepository.GetAllAsync();

            var ordersModelList = orders.Select(x => new OrdersResponce(x)).ToList();

            return ordersModelList;
        }

        /// <summary>
        /// Получить заказ по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<OrdersResponce> GetOrderByIdAsync(Guid id)
        {
            var order = await _cache.GetByIdAsync(id);

            return new OrdersResponce(order);
        }


        /// <summary>
        /// Создать заказ от клиента с id
        /// </summary>
        /// <param name="customerId"><example>123456d6-1111-4a11-9c7b-eb9f14e1a111</example></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<OrdersResponce>> CreateOrderFromCustomer(Guid customerId)

        {
            Order order = new Order()
            {
                Id = Guid.NewGuid(),
                Comment = "",
                CustomerId = customerId,
                Status = OrderStatus.Created,
                CreationDate = DateTime.Now,
                Products = new List<Product>()
            };

            await _cache.CreateAsync(order);

            //TODO: разобраться с передачей вот здесь
            //await _givingOrderDeliveryGateway.GiveOrderToAll(order);

            return Ok(new OrdersResponce(order));
        }

        /// <summary>
        /// Изменить статус заказа на Completed
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("/completed")]
        public async Task<ActionResult> СhangeStatusOrderAsync(Guid id)
        {
            var order = await _cache.GetByIdAsync(id);

            order.Status = OrderStatus.Completed;

            await _cache.UpdateAsync(order);

            return Ok("Статус заказа изменен на Completed");
        }

        /// <summary>
        /// Удалить заказ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteOrderAsync(Guid id)
        {
            var order = await _cache.GetByIdAsync(id);

            if (order == null) return BadRequest(new { message = "Incorrect id" });

            await _cache.DeleteAsync(id);

            return Ok("Заказ удален");
        }
    }
}
