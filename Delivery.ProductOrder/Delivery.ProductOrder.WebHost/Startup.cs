using Delivery.ProductOrder.Core.Abstractions;
using Delivery.ProductOrder.DataAccess;
using Delivery.ProductOrder.DataAccess.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using Microsoft.Extensions.Hosting;
using System;
using Delivery.ProductOrder.Integration;
using Delivery.ProductOrder.DataAccess.Services;
using Blocks.Contracts.Abstractions;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductOrder.Integration.AsyncDataServices;
using Delivery.ProductOrder.Integration.EventProcessing;

namespace Delivery.ProductOrder.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

            services.AddDbContext<DataContext>(x => {

                x.UseNpgsql(Configuration.GetConnectionString("ProductOrderConnection"));

            }, ServiceLifetime.Singleton);

            services.AddScoped<ICachingService<Order>, OrderService>();
            services.AddScoped<ICachingService<Customer>, CustomerService>();

            services.AddHostedService<MessageBusSubscriber>();

            services.AddMemoryCache();

            services.AddSingleton<IEventProcessor, EventProcessor>();

            services.AddControllers();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "Delivery.ProductOrder API";
                options.Version = "1.0";
            });

            services.AddCors(option =>
            {
                option.AddDefaultPolicy(
                    policy =>
                    {
                        policy.WithOrigins("http://localhost:3000",
                                "https://localhost:3000", "http://localhost:3001",
                                "https://localhost:3001", "http://localhost:3002",
                                "https://localhost:3002")
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseCors();

            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
