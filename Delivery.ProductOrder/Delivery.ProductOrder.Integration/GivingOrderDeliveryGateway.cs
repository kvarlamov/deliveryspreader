﻿using Delivery.ProductOrder.Core.Abstractions;
using Delivery.ProductOrder.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Blocks.Contracts.OrdersManagement;

namespace Delivery.ProductOrder.Integration
{
    public class GivingOrderDeliveryGateway : IGivingOrderDeliveryGateway
    {
        private readonly HttpClient _httpClient;

        public GivingOrderDeliveryGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task GiveOrderToAll(Order order)
        {
            List<Guid> productList = new List<Guid>();
            foreach (var item in order.Products)
            {
                var productId = item.Id;
                productList.Add(productId);
            }
            var dto = new GiveOrderToAllDto()
            {
                OrderId = order.Id,
                CustomerId = order.CustomerId.Value,
                Comment = order.Comment,
                ProductsId = productList,
                CreationDate = order.CreationDate,
                IsPaid = order.IsPaid
            };

            var response = await _httpClient.PostAsJsonAsync("api/v1/orders", dto);

            response.EnsureSuccessStatusCode();
        }
    }
}
