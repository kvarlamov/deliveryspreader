﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.Integration.Dto
{
    public class GiveOrderToAllDto
    {
        public Guid OrderId { get; set; }

        public Guid CustomerId { get; set; }

        public List<Guid> ProductsId { get; set; }

        public bool IsPaid { get; set; }

        public string Comment { get; set; }

        public DateTime CreationDate { get; set; }

    }
}
