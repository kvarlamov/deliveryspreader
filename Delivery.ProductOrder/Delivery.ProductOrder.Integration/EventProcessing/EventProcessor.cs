﻿
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text.Json;
using Delivery.ProductOrder.DataAccess.Services;
using Blocks.Contracts.Dto;
using Blocks.Contracts.OrdersManagement;
using Blocks.Helpers;
using Blocks.Contracts.Abstractions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.Integration.EventProcessing
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IServiceProvider _serviceProvider;

        public EventProcessor(IServiceScopeFactory scopeFactory, IServiceProvider services)
        {
            _scopeFactory = scopeFactory;
            _serviceProvider = services;
        }

        public void ProcessEvent(string message)
        {
            var eventType = EventHelper.DetermineEvent(message);

            switch (eventType)
            {
                case EventType.Product_Sent:
                    addProductToOrder(message);
                    break;
                case EventType.ProductRange_Sent:
                    addProductRangeToOrder(message);
                    break;
                case EventType.Courier_Assigned:
                    assignCourier(message);
                    break;
                case EventType.Undetermined:
                    throw new InvalidOperationException("Event not determined");
                    break;
            }
        }

        private async void addProductToOrder(string notificationMessage)
        {
            Console.WriteLine("--> Starting courier assigning");

            var orderRepo = _serviceProvider.GetService<ICachingService<Order>>();

            Console.WriteLine($"--> got order service: {orderRepo}");

            var dto = JsonSerializer.Deserialize<AddProductDto>(notificationMessage);
            Console.WriteLine("--> Product deserialized");

            try
            {
                var order = await orderRepo.GetByIdAsync(dto.OrderId.Value);
                FillProducts(dto, order);

                await orderRepo.UpdateAsync(order);
                Console.WriteLine($"--> Added products");
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Could not add products: {e.Message}");
                throw;
            }


        }

        private static Order FillProducts(AddProductDto dto, Order order)
        {
            if (order.Products is null)
            {
                order.Products = new List<Product>();
            }

            bool orderContainsProduct = order.Products.Select(product => product.Id).ToList().Contains(dto.ProductId.Value);

            if (orderContainsProduct)
            {
                order.Products.
                    Where(p => p.Id == dto.ProductId.Value).
                    First().Amount += dto.Amount;
            }
            else
            {
                order.Products.Add(
                new Product
                {
                    Id = dto.ProductId.Value,
                    Name = dto.Name,
                    Price = dto.Price,
                    DeliveryPrice = dto.DeliveryPrice,
                    Amount = dto.Amount
                });
            }
            return order;
        }

        private async void addProductRangeToOrder(string notificationMessage)
        {
            Console.WriteLine("--> Starting product adding");

            var orderRepo = _serviceProvider.GetService<ICachingService<Order>>();

            Console.WriteLine($"--> got order service: {orderRepo}");

            var dtos = JsonSerializer.Deserialize<AddProductRangeDto>(notificationMessage);
            Console.WriteLine("--> Product deserialized");

            try
            {
                var order = await orderRepo.GetByIdAsync(dtos.OrderId);
                var dtoList = dtos.DtoCollection;

                foreach (var dto in dtoList)
                {
                    order = FillProducts(dto, order);
                }

                await orderRepo.UpdateAsync(order);
                Console.WriteLine($"--> Added products");
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Could not add products: {e.Message}");
                throw;
            }


        }


        private async void assignCourier(string notificationMessage) {

            Console.WriteLine("--> Starting courier assigning");

            var orderRepo = _serviceProvider.GetService<ICachingService<Order>>();


            Console.WriteLine($"--> got order service: {orderRepo}");

                var dto = JsonSerializer.Deserialize<AssignCourierDto>(notificationMessage);
                Console.WriteLine("--> Courier deserialized");
            try
            {
                var order = await orderRepo.GetByIdAsync(dto.OrderId);
                

                order.CourierId = dto.CourierId;
                order.Status = OrderStatus.CourierAssigned;
                order.Courier = new Courier { 
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    Email = dto.Email};
               
                
                await orderRepo.UpdateAsync(order);
                Console.WriteLine($"--> Assigned Courier");
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Could not assign courier: {e.Message}");
                throw;
            }
            }
        }

    }
