﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.Integration.EventProcessing
{
    public interface IEventProcessor
    {
        void ProcessEvent(string message);
    }
}
