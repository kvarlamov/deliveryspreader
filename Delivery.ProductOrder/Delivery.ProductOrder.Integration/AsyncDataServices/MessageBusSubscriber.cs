﻿using Delivery.ProductOrder.Integration.EventProcessing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.Integration.AsyncDataServices
{
    public class MessageBusSubscriber : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IEventProcessor _eventProcessor;
        private IConnection _connection;
        private IModel _channel;

        public MessageBusSubscriber(IConfiguration configuration, IEventProcessor eventProcessor)
        {
            _configuration = configuration;
            _eventProcessor = eventProcessor;
            InitializeRabbitMQ();
        }

        private void InitializeRabbitMQ()
        {
            var factory = new ConnectionFactory()
            {
                UserName = "admin",
                Password = "admin",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare(exchange: "courierNotification", type: ExchangeType.Fanout);
            _channel.ExchangeDeclare(exchange: "productExchange", type: ExchangeType.Fanout);

            Console.WriteLine("--> Connected to MessageBus");
            _channel.QueueDeclare(queue: "courierAssigned",
                         durable: false,
                         exclusive: false,
                         autoDelete: false,
                         arguments: null);
            _channel.QueueBind(queue: "courierAssigned",
                              exchange: "courierNotification",
                              routingKey: "");

            _channel.QueueDeclare(queue: "productSendingQueue",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);
            _channel.QueueBind(queue: "productSendingQueue",
                                      exchange: "productExchange",
                                      routingKey: "");


        }


        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }

            base.Dispose();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, args) =>
            {
                Console.WriteLine("--> Received Message");
                var body = args.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());

                _eventProcessor.ProcessEvent(message);
                Console.WriteLine($"--> Message contents: {message}");

            };

            _channel.BasicConsume(queue: "courierAssigned",
                                    autoAck: true, consumer);
            _channel.BasicConsume(queue: "productSendingQueue",
                                    autoAck: true, consumer);

            return Task.CompletedTask;
        }
    }
}
