﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Delivery.ProductOrder.DataAccess.Data
{
    public static class FakeDataFactory
    {

        public static IEnumerable<Order> Orders => new List<Order>()
        {
            new Order()
            {
                Id = Guid.Parse("123456d6-d8d5-1111-9c7b-eb9f14e1a32f"),
                Status = OrderStatus.New,
                IsPaid = true,
                CreationDate = DateTime.Now,
                CourierAssignedDate = null,
                CompletionDate = null,
                Comment = "Комментарий хороший",
                CustomerId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111"),
                Products = new List<Product>()
            },
            new Order()
            {
                Id = Guid.Parse("223456d6-d8d5-2222-9c7b-eb9f14e1a32f"),
                Status = OrderStatus.New,
                IsPaid = true,
                CreationDate = DateTime.Now,
                CourierAssignedDate = null,
                CompletionDate = null,
                Comment = "Комментарий плохой",
                CustomerId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111"),
                Products = new List<Product>()
            },
            new Order()
            {
                Id = Guid.Parse("323456d6-d8d5-3333-9c7b-eb9f14e1a32f"),
                Status = OrderStatus.New,
                IsPaid = true,
                CreationDate = DateTime.Now,
                CourierAssignedDate = null,
                CompletionDate = null,
                Comment = "Комментарий",
                CustomerId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111"),
                Products = new List<Product>()
            }
        };


        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new Customer()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111"),
                FirstName = "Василий",
                Address = "Тверская, 19",
                Email = "vv@vv.ru",
                Orders = Orders.
                Where(o => 
                    o.CustomerId == Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111")).
                ToList()
                
            },
            new Customer()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a222"),
                FirstName = "Генадий",
                Address = "Светлая, 82",
                Email = "gg@gg.ru",
                Orders = Orders.
                Where(o =>
                    o.CustomerId == Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a222")).
                ToList()
            },
            new Customer()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a333"),
                FirstName = "Кирилл",
                Address = "Добролюбова, 14",
                Email = "kk@kk.ru",
                Orders = Orders.
                Where(o =>
                    o.CustomerId == Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a333")).
                ToList()
            }
        };

    }
}
