﻿using Microsoft.EntityFrameworkCore;
using Delivery.ProductOrder.DataAccess.Data;
using Blocks.Contracts.OrdersManagement;

namespace Delivery.ProductOrder.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
           : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();

            Customers.AddRange(FakeDataFactory.Customers);
            this.SaveChanges();

        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ConfigureWarnings(warnings =>
    warnings.Default(WarningBehavior.Ignore));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Инициализация базы данных

            modelBuilder.Entity<Order>().
                HasOne<Customer>(c => c.Customer).
                WithMany(o => o.Orders).
                HasForeignKey(c => c.CustomerId);

            modelBuilder.Ignore<Courier>();
            modelBuilder.Ignore<Product>();

        }
    }
}
