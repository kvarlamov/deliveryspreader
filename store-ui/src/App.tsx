import React from 'react';
import './App.css';
import StockItem from './StockItem';

function App() {
    return (
        <div className="App">
            <StockItem />
        </div>
    );
}

export default App;
