﻿using Blocks.Contracts.OrdersManagement;
using System.Threading.Tasks;

namespace Delivery.ProductStore.Core.Abstractions
{
    public interface IGivingProductDeliveryGateway
    {
        Task GiveProductToAll(Product product);
    }
}
