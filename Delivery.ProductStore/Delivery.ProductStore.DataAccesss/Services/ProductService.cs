﻿using Blocks.Contracts.Abstractions;
using Blocks.Contracts.Models;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductStore.Core.Abstractions;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace Delivery.ProductStore.WebHost.Services
{
    public class ProductService: ICachingService<Product>
    {
        private readonly IRepository<Product> _repository;
        private readonly IMemoryCache _cache;

        public ProductService()
        {
        }

        public ProductService(IRepository<Product> repository, IMemoryCache cache)
        {
            _repository = repository;
            _cache = cache;
        }

        public async Task<Product> CreateAsync(Product product)
        {


            await _repository.CreateAsync(product);

            _cache.Set(product.Id, product, new MemoryCacheEntryOptions { 
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            return product;
        }

        public async Task<Product> GetByIdAsync(Guid id)
        {
            Product product;
            if (!_cache.TryGetValue(id, out product))
            {
                product = await _repository.GetByIdAsync(id);
                if (product != null)
                {
                    _cache.Set(id, product, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                    });
                }

            }
            return product;
        }

        public async Task UpdateAsync(Product product)
        {

            _cache.Set(product.Id, product, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            await _repository.UpdateAsync(product);

        }

        public async Task DeleteAsync(Guid id)
        {
            
            var product = await this.GetByIdAsync(id);
            await _repository.DeleteAsync(product);
            _cache.Remove(id);

        }

        Task<Product> ICachingService<Product>.GetByIdAsync(Guid id, string property)
        {
            throw new NotImplementedException();
        }
    }
}
