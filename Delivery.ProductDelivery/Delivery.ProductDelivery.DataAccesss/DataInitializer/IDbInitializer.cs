﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delivery.ProductDelivery.DataAccesss.DataInitializer
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
