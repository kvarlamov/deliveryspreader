﻿using Microsoft.EntityFrameworkCore;
using Delivery.ProductDelivery.DataAccess.Data;
using Blocks.Contracts.OrdersManagement;

namespace Delivery.ProductDelivery.DataAccess
{
    public class DataContext : DbContext
    {

        public DbSet<Courier> Couriers { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();

            Couriers.AddRange(FakeDataFactory.Couriers);
            this.SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Инициализация базы данных

        }
    }
}
