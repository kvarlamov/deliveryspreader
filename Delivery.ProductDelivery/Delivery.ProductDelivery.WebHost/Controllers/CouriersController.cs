﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Delivery.ProductDelivery.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blocks.Contracts.OrdersManagement;
using Blocks.Contracts.Models;
using Blocks.Contracts.Abstractions;
using Delivery.ProductDelivery.Integration.AsyncDataServices;
using Blocks.Contracts.Dto;

namespace Delivery.ProductDelivery.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]

    public class CouriersController : ControllerBase
    {

        private readonly ILogger<CouriersController> _logger;
        private readonly IRepository<Courier> _courierRepository;
        private readonly ICachingService<Courier> _service;
        private readonly IMessageBusClient _messageBus;

        public CouriersController(ILogger<CouriersController> logger, 
            IRepository<Courier> courierRepository, 
            ICachingService<Courier> service,
            IMessageBusClient messageBus)
        {
            _logger = logger;
            _courierRepository = courierRepository;
            _service = service;
            _messageBus = messageBus;
        }

        /// <summary>
        /// Получить всех курьеров
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CouriersResponse>> GetCouriersAsync()
        {
            var couriers = await _courierRepository.GetAllAsync();

            var couriersModelList = couriers.Select(x => new CouriersResponse(x)).ToList();

            return couriersModelList;
        }

        /// <summary>
        /// Получить курьера по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<CouriersResponse> GetCourierByIdAsync(Guid id)
        {
            var courier = await _service.GetByIdAsync(id);
            return new CouriersResponse(courier);
        }

        /// <summary>
        /// Создать курьера
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Courier> CreateCourierAsync(CourierRequest request)
        {
            Courier courier = new Courier
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };
            await _service.CreateAsync(courier);
            return courier;
        }

        /// <summary>
        /// Обновить данные курьера
        /// </summary>
        /// <param name="courier"></param>
        /// <returns></returns>
        [HttpPut("courier")] //пока сделал так чтобы работал метод put ниже
        public async Task<ActionResult> UpdateCourierAsync(Courier courier)
        {
            await _service.UpdateAsync(courier);
            return Ok("Курьер обновлен");
        }

        /// <summary>
        /// Назначить курьера на заказ
        /// </summary>
        /// <param name="id"><example>323456d6-d8d5-3333-1111-eb9f14e1a32f</example></param>
        /// <param name="orderId"><example>123456d6-d8d5-1111-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> AssignOrder(Guid id, Guid orderId)
        {
            var courier = await _service.GetByIdAsync(id);
            if (courier is null)
            {
                return NotFound();
            }

            if (courier.OrderIds is null)
            {
                courier.OrderIds = new List<Guid>();
            }
            courier.OrderIds.Add(orderId);

            var dto = new AssignCourierDto(courier, orderId);
            _messageBus.AssignCourierToOrder(dto);
            
            await _service.UpdateAsync(courier);
            return Ok("Заказ добавлен");
        }

        /// <summary>
        /// Удалить курьера
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteCourierAsync(Guid id)
        {
            await _service.DeleteAsync(id);
            return Ok("Курьер удален");
        }
    }
}
