﻿using Blocks.Contracts.Dto;

namespace Delivery.ProductDelivery.Integration.AsyncDataServices
{
    public interface IMessageBusClient
    {
        void AssignCourierToOrder(AssignCourierDto dto);

        
    }
}
