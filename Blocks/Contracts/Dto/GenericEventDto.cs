﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blocks.Contracts.Dto
{
    public class GenericEventDto
    {
        public string Event { get; set; }
    }
}
