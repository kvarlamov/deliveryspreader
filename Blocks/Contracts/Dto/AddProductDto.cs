﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Blocks.Contracts.Dto
{
    public class AddProductDto
    {
        [JsonConstructor]
        public AddProductDto() { }

        public AddProductDto(Product product, Guid orderId, int amount)
        {
            ProductId = product.Id;
            OrderId = orderId;
            Event = "Product_Sent";
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            DeliveryPrice = product.DeliveryPrice;
            if (amount > product.Amount)
            {
                Amount = product.Amount;
            }
            else Amount = amount;
        }

        public Guid? ProductId { get; set; }
        public Guid? OrderId { get; set; }

        public string Event { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public int DeliveryPrice { get; set; }

        public int Amount { get; set; }
    }
}
