﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blocks.Contracts.Dto 
{ 

    public class GiveProductToAllDto
    {
        public Guid ProductId { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public int DeliveryPrice { get; set; }
    }
}
