﻿using System;

namespace Blocks.Contracts.BaseEntitys
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
