﻿using Blocks.Contracts.BaseEntitys;
using System;
using System.Collections.Generic;

namespace Blocks.Contracts.OrdersManagement
{
    public enum OrderStatus
    {
        New,
        Created,
        CourierAssigned,
        InProcess,
        Completed,
        Canceled
    }
    public class Order : BaseEntity
    {
        public OrderStatus Status { get; set; }

        public bool IsPaid { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? CourierAssignedDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        public string Comment { get; set; }

        public Guid? CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid? CourierId { get; set; }

        public Courier Courier { get; set; }

        public virtual ICollection<Product> Products { get; set; }

    }
}
