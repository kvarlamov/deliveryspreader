﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blocks.Contracts.Models
{
    public class CustomersResponse
    {
        public CustomersResponse(Customer customer)
        {
            Id = customer.Id;
            FirstName = customer.FirstName;
            Email = customer.Email;
            Address = customer.Address;
            if (customer.Orders != null)
            {
                Orders = customer.Orders.Select(o => new OrderShortResponse(o)).ToList();
            }
        }

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public List<OrderShortResponse> Orders { get; set; }
    }
}
