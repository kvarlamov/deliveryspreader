namespace Blocks.Contracts.Models
{
    public class CourierRequest
    {
        public CourierRequest(string firstName, string secondName, string email)
        {
            FirstName = firstName;
            LastName = secondName;
            Email = email;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
