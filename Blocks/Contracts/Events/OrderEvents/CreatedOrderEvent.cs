﻿using Blocks.Contracts.Events;
using Blocks.Contracts.OrdersManagement;

namespace Blocks.Contracts
{
    public class CreatedOrderEvent : MainEvent
    {
        public Order Orders { get; set; }
        public CreatedOrderEvent(Order content)
        {
            Orders = content;
        }
    }
}
