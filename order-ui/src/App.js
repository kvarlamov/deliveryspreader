import classes from './App.module.css'
import Layout from "./hoc/Layout/Layout";
import {Route, Routes, Navigate} from "react-router-dom";
import ProductList from "./components/ProductList/ProductList";
import About from "./components/About/About";
import Auth from "./components/Auth/Auth";
import React, {Component} from "react";
import Cart from "./components/Cart/Cart";
import {ProductDetails} from "./components/ProductDetails/ProductDetails";

class App extends Component {
    state = {
        authorized: true
        //todo - change to false
    }

    onAuthorizationChange = () => {
        this.setState({authorized: true});
        //todo: logout
    }
    
    render() {
        return (
            <Layout>
                <Routes>
                     }
                    <Route path='/auth' exact element={<Auth onAuthorizationChange={this.onAuthorizationChange}/>}/>
                    <Route path='/cart' exact element={this.state.authorized ? <Cart/> : <Navigate to='/auth' />} />
                    <Route path='/about' exact element={this.state.authorized ? <About/> : <Navigate to='/auth' />} />
                    <Route path='/' exact element={this.state.authorized ? <ProductList/> : <Navigate to='/auth' />}/>
                    <Route path='/product/:id' exact element={this.state.authorized ? <ProductDetails/> : <Navigate to='/auth' />}/>
                </Routes>
            </Layout>
        );
    }
}

export default App;
