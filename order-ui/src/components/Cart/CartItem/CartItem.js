﻿import React, {useState} from "react";
import styles from './CartItem.module.css'

export const CartItem = ({product, amount, className}) => {
    const cls = [styles.CartItem, styles[className]]
    
    return (
        <div className={cls.join(' ')}>
            <img src={require('../../Product/images/test.jpg')}/>
            <p>{product.name}</p>
            <p>{product.price}</p>
            <p>{amount} шт.</p>
        </div>
    )
}